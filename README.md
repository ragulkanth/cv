# cv

This repo is to track and build my resume using LaTex. My resume is based on the template called [Awesome-CV](https://github.com/posquit0/Awesome-CV).

## To build the PDF 

At the command prompt, run

```bash
xelatex ragulkanth-cv.tex
```
